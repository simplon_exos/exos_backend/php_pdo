-- On supprime la base de données si elle existe
DROP DATABASE IF EXISTS introduction;
-- On crée la base de données introduction
CREATE DATABASE introduction;
-- On sélectionne la base de données introduction
USE introduction;

-- On crée une table person
CREATE TABLE person( 
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    name VARCHAR(64), 
    age INT, 
    personality VARCHAR(64) 
);
-- On crée une table school
CREATE TABLE school(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(64), 
    type VARCHAR(64), 
    specialty VARCHAR(64) 
);
-- Création de la table address
CREATE TABLE address(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    number VARCHAR(5),
    street VARCHAR(128),
    city VARCHAR(64)
);
-- On crée la table de jointure pour le ManyToMany entre person et address
CREATE TABLE person_address(
    person_id INT NOT NULL,
    address_id INT NOT NULL
);

-- On modifie la table person pour y rajouter un champ school_id
ALTER TABLE person ADD school_id INT;
-- On modifie la table person pour indiquer que le champ school_id 
-- est une clef étrangère faisant référence au champ id de la table school
ALTER TABLE person ADD FOREIGN KEY (school_id) REFERENCES school(id);

-- On indique que les deux champs de la table de jointure sont des clefs
-- étrangère faisant référence à la table person et la table address
ALTER TABLE person_address ADD FOREIGN KEY (person_id) REFERENCES person(id);
ALTER TABLE person_address ADD FOREIGN KEY (address_id) REFERENCES address(id);


-- On ajoute un jeu de données par défaut dans les tables
INSERT INTO school (name,type,specialty) VALUES ('Simplon', 'professional', 'web');
INSERT INTO school (name,type,specialty) VALUES ('Jacques Brel', 'high school', 'general');
INSERT INTO school (name,type,specialty) VALUES ('Dutronc', 'high school', 'woodwork');

INSERT INTO person (name,personality,age,school_id) VALUES ('Jean', 'serious', 32, 1);
INSERT INTO person (name,personality,age,school_id) VALUES ('Pierre', 'cool', 30, 1);
INSERT INTO person (name,personality,age,school_id) VALUES ('Paula', 'joker', 16, 2);
INSERT INTO person (name,personality,age,school_id) VALUES ('Steve', 'sassy', 18, 3);
INSERT INTO person (name,personality,age,school_id) VALUES ('Laura', 'serious', 20, 3);
INSERT INTO person (name,personality,age,school_id) VALUES ('Lisa', 'cool', 40, NULL);

INSERT INTO address (number,street,city) VALUES ('11', 'Georges Rivet street', 'Vénissieux');
INSERT INTO address (number,street,city) VALUES ('2', 'fake street', 'Fake City');
INSERT INTO address (number,street,city) VALUES ('45', 'Garibaldi street', 'Lyon');

INSERT INTO person_address (person_id, address_id) VALUES (1, 1);
INSERT INTO person_address (person_id, address_id) VALUES (2, 1);
INSERT INTO person_address (person_id, address_id) VALUES (3, 2);
INSERT INTO person_address (person_id, address_id) VALUES (3, 3);
